# Uncertainty in ensembles of global biodiversity scenarios
### W. Thuiller, M. Guéguen, J. Renaud, D. N. Karger and N. E. Zimmermann

This folder contains all simulation code, to :

1. prepare data
2. run Species Distribution Models
3. get and format results
4. calculate outputs
5. create graphics
6. get table of results


The file *GUIDELINES_README.pdf* is a summary table containing, for each step of the workflow (meaning each script) :
- the names of the files that are used,
- and the names of the files/graphics that are produced.

